import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        forSale: [
            { invId: 1, name: 'CPU Intel Pentium G4600 3.6GHz', image: '\\src\\img\\cpu.png', price: 9999 },
            { invId: 2, name: 'CPU Intel Core i3-8100 3.6GHz', image: '\\src\\img\\cpu3.png', price: 49999 },
            { invId: 3, name: 'Intel® Core™ i5-7400 3.0-3.5GHz ', image: '\\src\\img\\cpu5.png', price: 69999 },
            { invId: 4, name: 'Intel® Core™ i7-7700 3.6-4.2GHz ', image: '\\src\\img\\cpu7.png', price: 99999 },
            { invId: 5, name: 'Biostar RACING B250GT3, Socket 1151, Dual 4xDDR4-2400', image: '\\src\\img\\mother1.jpg', price: 12500 },
            { invId: 6, name: 'MB S2066 Asus PRIME X299-A, Socket 1151 ', image: '\\src\\img\\mother2.jpg', price: 31599 },
            { invId: 7, name: 'ASUS ROG STRIX Z370-F GAMING, Socket 1151', image: '\\src\\img\\mother3.png', price: 9999 },
            { invId: 8, name: 'MB S1151 Gigabyte GA-Z370 HD3 1.0 (Intel Z370, ATX)', image: '\\src\\img\\mother4.jpg', price: 45900 },
            { invId: 9, name: 'CPUSapphire PULSE Radeon RX 550 2GB DDR5', image: '\\src\\img\\vga1.png', price: 35779 },
            { invId: 10, name: 'MSI GeForce GTX 1050 GAMING X 2G / 2GB DDR5', image: '\\src\\img\\vga2.png', price: 87999 },
            { invId: 11, name: 'GA card PCI-E Gigabyte GV-N105TD5-4GD 1.1', image: '\\src\\img\\vga3.png', price: 102589 },
            { invId: 12, name: 'VGA card PCI-E Gigabyte GV-N108TAORUS-11GD', image: '\\src\\img\\vga4.png', price: 157999 },
            { invId: 13, name: '2.5" HDD 1.0TB Seagate Barracuda', image: '\\src\\img\\hdd1.png', price: 10999 },
            { invId: 14, name: '3.5" HDD 3.0TB Seagate Barracuda', image: '\\src\\img\\hdd2.jpg', price: 21299 },
            { invId: 15, name: '3.5" HDD 8.0TB Western Digital', image: '\\src\\img\\hdd3.jpg', price: 82999 },
            { invId: 16, name: '3.5" HDD 10.0TB Western Digital', image: '\\src\\img\\hdd4.png', price: 97499 },
        ],
        inCart: [],
    },
    getters: {
        forSale: state => state.forSale,
        inCart: state => state.inCart,
    },
    mutations: {
        ADD_TO_CART(state, invId) { state.inCart.push(invId); },
        REMOVE_FROM_CART(state, index) { state.inCart.splice(index, 1); },
    },
    actions: {
        addToCart(context, invId) { context.commit('ADD_TO_CART', invId); },
        removeFromCart(context, index) { context.commit('REMOVE_FROM_CART', index); },
    },
});
